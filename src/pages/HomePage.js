import { useNavigate } from "react-router-dom";
import DealOfTheWeek from "../components/DealOfTheWeek/DealOfTheWeek";
import NewProduct from "../components/Products/NewProduct";
import Slide from "../components/Slide/Slide";

function HomePage() {

    const navigate = useNavigate();
    const handleClickShop = () => {
        navigate('/products')
    }
    return (
        <>
            <Slide />
            <div className="product-list">
                <div className="container">
                    <div className="section-heading">
                        <h3>Sản phẩm mới</h3>
                    </div>
                    <NewProduct />
                    <div className="section-view-all">
                        <button onClick={handleClickShop}>Xem thêm...</button>
                    </div>
                </div>
            </div>
            <DealOfTheWeek/>
        </>
    )
}
export default HomePage;