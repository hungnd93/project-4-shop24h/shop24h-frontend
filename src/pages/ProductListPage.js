import React from "react";
import BreadCrumb from "../components/BreadCrumb/BreadCrumb";
import ProductFilter from "../components/Products/ProductFilter";
import ProductList from "../components/Products/ProductList ";

function ProductListPage() {

    const crumbs = [{
        name: 'Trang chủ',
        url: '/'
    }, {
        name: 'Danh mục sản phẩm',
        url: '/products'
    }];

    return (
        <React.Fragment>
            <div className="container">
                <div>
                    <BreadCrumb crumbs={crumbs} />
                </div>
                <div className="all-product-list">
                    <ProductFilter />
                    <ProductList />
                </div>
            </div>
        </React.Fragment>
    )
}
export default ProductListPage;