// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getAuth } from "firebase/auth";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBnwtZAhMV127Jpt4TOmJDv0I7_du57hpw",
  authDomain: "loginshop24h.firebaseapp.com",
  projectId: "loginshop24h",
  storageBucket: "loginshop24h.appspot.com",
  messagingSenderId: "227817299067",
  appId: "1:227817299067:web:c7fc88d8da03015d1696de"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export default auth;