import { Input, Row, Label, FormGroup, Button, Col } from "reactstrap";
import { useState } from "react"

function FormRegister() {
    
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [emailSignUp, setEmailSingUp] = useState("");
    const [passwordSignUp, setPasswordSignUp] = useState("");

    const changeFirstNameHandler = (event) => {
        setFirstName(event.target.value);
    };

    const changeLastNameHandler = (event) => {
        setLastName(event.target.value);
    };

    const changeSignUpEmailHandler = (event) => {
        setEmailSingUp(event.target.value);
    };

    const changeSignUpPasswordHandler = (event) => {
        setPasswordSignUp(event.target.value);
    };
    const buttonSignUp = () => {
        if (firstName === "") {
            alert("First Name is not valid");
            return false;
        }
        if (lastName === "") {
            alert("Last Name is not valid");
            return false;
        }
        if (emailSignUp === "") {
            alert("Email is not valid");
            return false;
        }
        if (passwordSignUp === "") {
            alert("Password is not valid");
            return false;
        }
        return true;
    };

    return (
        <>
            <div>
                <Row className="text-center mt-5 mb-3">
                    <h2>
                        Sign Up For Free
                    </h2>
                </Row>
                <Row>
                    <Col sm={6}>
                        <FormGroup floating>
                            <Input

                                id="firstname"
                                placeholder="name"
                                type="name"
                                style={{ backgroundColor: "#fff"}}
                                onChange={changeFirstNameHandler}
                                value={firstName}
                            />
                            <Label for="firstname">
                                First Name <span style={{ color: "red" }}>*</span>
                            </Label>
                        </FormGroup>
                    </Col>
                    <Col sm={6}>
                        <FormGroup floating>
                            <Input
                                id="lastname"
                                placeholder="name"
                                type="name"
                                style={{ backgroundColor: "#fff" }}
                                onChange={changeLastNameHandler}
                                value={lastName}
                            />
                            <Label for="lastname">
                                Last Name <span style={{ color: "#red" }}>*</span>
                            </Label>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <FormGroup floating>
                        <Input
                            id="Email"
                            placeholder="Email"
                            type="email"
                            style={{ backgroundColor: "#fff"}}
                            onChange={changeSignUpEmailHandler}
                            value={emailSignUp}
                        />
                        <Label for="Email" style={{ marginLeft: "10px" }}>
                            Email Address <span style={{ color: "red" }}>*</span>
                        </Label>
                    </FormGroup>
                </Row>
                <Row>
                    <FormGroup floating>
                        <Input
                            id="Password"
                            placeholder="password"
                            type="password"
                            style={{ backgroundColor: "#fff" }}
                            onChange={changeSignUpPasswordHandler}
                            value={passwordSignUp}
                        />
                        <Label for="Password" style={{ marginLeft: "10px" }}>
                            Set Password <span style={{ color: "red" }}>*</span>
                        </Label>
                    </FormGroup>
                </Row>
                <Row className="mt-3">
                    <div >
                        <Button style={{ backgroundColor: "#1877f2", width: "100%" }}><h3 className="mb-0" onClick={buttonSignUp}>Get Started</h3></Button>
                    </div>
                </Row>
            </div>
        </>
    )
}
export default FormRegister;