import { GoogleAuthProvider, onAuthStateChanged, signInWithPopup } from "firebase/auth";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Input, Row, Label, FormGroup, Button, Col } from "reactstrap";
import auth from "../../firebase";

const provider = new GoogleAuthProvider();

const FormLogin = () => {
    const [email, setEmailChange] = useState("");
    const [password, setPasswordChange] = useState("");
    const onChangeEmail = (event) => {
        setEmailChange(event.target.value);
    }
    const onChangePassword = (event) => {
        setPasswordChange(event.target.value)
    }
    const onBtnLogin = () => {
        if ((email !== "") && (password !== "")) {
            return true;
        }
        else {
            alert("Check email and password again");
            return false;
        }
    }

    const navigate = useNavigate();

    const [user, setUser] = useState(null);

    const loginGoogle = () => {
        signInWithPopup(auth, provider)
            .then((result) => {
                setUser(result);
                navigate('/');
            })
            .catch((error) => {
                console.error(error);
            })
    }
    //lưu lại giá trị user khi reload trang
    useEffect(() => {
        onAuthStateChanged(auth, (result) => {
            setUser(result);                 
        })
        
    })

    return (
        <>
            <div>
                <Row className="text-center mt-5 mb-3">
                    <h2>
                        Welcome Back!
                    </h2>
                </Row>
                <Row>
                    <FormGroup floating>
                        <Input
                            id="Email"
                            placeholder="Email"
                            type="email"
                            style={{ backgroundColor: "#fff" }}
                            onChange={onChangeEmail}
                            value={email}
                        />
                        <Label for="Email" style={{ marginLeft: "10px" }}>
                            Email Address <span style={{ color: "red" }}>*</span>
                        </Label>
                    </FormGroup>
                </Row>
                <Row>
                    <FormGroup floating>
                        <Input
                            id="Password"
                            placeholder="password"
                            type="password"
                            style={{ backgroundColor: "#fff" }}
                            onChange={onChangePassword}
                            autoComplete="current-password"
                            value={password}
                        />
                        <Label for="Password" style={{ marginLeft: "10px" }}>
                            Password <span style={{ color: "red" }}>*</span>
                        </Label>
                    </FormGroup>
                </Row>
                <Row >
                    <div className="d-flex justify-content-end">
                        <a href="#" style={{ textDecoration: "none" }}>Forgot Password?</a>
                    </div>
                </Row>
                <Row className="mt-3">
                    <div >
                        <Button style={{ backgroundColor: "#1877f2", width: "100%" }}><h3 className="mb-0" onClick={onBtnLogin}>Login</h3></Button>
                    </div>
                </Row>
                <Row className="mt-5">
                    <hr />
                </Row>
                <div className="login-icon">
                    <button><i class="fab fa-facebook-f" style={{ color: "#1877F2" }}></i></button>
                    <button onClick={loginGoogle}><i class="fab fa-google-plus-g" style={{ color: "#ff6348" }}></i></button>
                    <button><i class="fab fa-twitter" style={{ color: "#1D9BF0" }}></i></button>
                </div>
            </div>

        </>
    )
}
export default FormLogin;