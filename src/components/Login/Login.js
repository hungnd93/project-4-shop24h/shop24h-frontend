import React, { useState } from "react";
import { Row, Button, ButtonGroup, Container } from "reactstrap";
import './Login.css';
import FormLogin from "./FormLogin";
import FormRegister from "./FormRegister";
import BreadCrumb from "../BreadCrumb/BreadCrumb";

const Login = () => {

    const [ActiveStatusLogin, setActiveStatusLog] = useState(true);

    const [ActiveStatusRegis, setActiveStatusReg] = useState(false);

    const [ColorLog, setColorLog] = useState("#1AB188");

    const [ColorReg, setColorReg] = useState("#435359");

    const LoginButton = () => {
        setActiveStatusLog(true);
        setActiveStatusReg(false);
        setColorLog("#1AB188")
        setColorReg("#435359")

    }

    const RegisterButton = () => {
        setActiveStatusReg(true)
        setActiveStatusLog(false);
        setColorReg("#1AB188")
        setColorLog("#435359")

    }

    const crumbs = [{
        name: 'Trang chủ',
        url: '/'
    },
    {
        name: 'Đăng nhập',
        url: '/login'
    }];

    return (
        <React.Fragment>
            <Container>
                <BreadCrumb crumbs={crumbs}/>
            </Container>
            <Container className="form-login">
                <Row>
                    <ButtonGroup>
                        <Button id="btn-login" style={{ backgroundColor: ColorLog }} onClick={LoginButton} active={ActiveStatusLogin}>
                            Log In
                        </Button>
                        <Button id="btn-register" style={{ backgroundColor: ColorReg }} onClick={RegisterButton} active={ActiveStatusRegis}>
                            Sign Up
                        </Button>
                    </ButtonGroup>
                </Row>
                {ActiveStatusLogin ?
                    <FormLogin />
                    :
                    <FormRegister />
                }
            </Container>
        </React.Fragment>
    )
}
export default Login;

