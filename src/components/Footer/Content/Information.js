import { Row } from "reactstrap"

function Information() {
    return (
        <>
        <div >
            <Row className="mb-4">
                <h4>THÔNG TIN</h4>
            </Row>
            <Row>
                <p>Trạng thái đơn hàng</p>
            </Row>
            <Row>
                <p>Hình thức giao hàng</p>
            </Row>
            <Row>
                <p>Hình thức thanh toán</p>
            </Row>
            <Row>
                <p>Chính sách đổi trả</p>
            </Row>
            <Row>
                <p>Chính sách bảo hành</p>
            </Row>
        </div>
        </>
    )
}
export default Information;