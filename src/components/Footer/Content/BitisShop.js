import { Button, Col, Input, Row } from "reactstrap";
import '@fortawesome/fontawesome-free/css/all.min.css';

function BitisShop() {
    return (
        <div>
            <Row className="mb-4">
                <h4><span style={{ color: "blue" }}>Biti's</span> <span style={{ color: "red" }}>SHOP</span></h4>
            </Row>
            <Row>
                <Input className="footer-form" placeholder="Enter your email"/>
            </Row>
            <Row>
                <Button className="footer-form" color="success">
                    SUBSCRIBE
                </Button>
            </Row>
            <Row className="mt-5">
                <Col><span style={{color: "blue"}}><i type="button" className="fa-brands fa-facebook "></i></span></Col>
                <Col><span style={{color: "#1D9BF0"}}><i type="button" className="fa-brands fa-twitter"></i></span></Col>
                <Col><span style={{color: "#0C64C5"}}><i type="button" className="fa-brands fa-linkedin"></i></span></Col>
                <Col><span style={{color: "red"}}><i type="button" className="fa-brands fa-youtube"></i></span></Col>
            </Row>
        </div>
    )
}
export default BitisShop;