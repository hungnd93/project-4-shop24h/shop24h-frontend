
import React from "react";
import { Col, Container, Row } from "reactstrap";
import About from "./Content/About";
import BitisShop from "./Content/BitisShop";
import Information from "./Content/Information";
import Support from "./Content/Support";
import './Footer.css'

function Footer() {

    return (
        <div className="main-footer">
            <footer className="container">
            <Container>
                <Row className="Footer__links">
                    <Col sm={3} className="footer__links">
                        <About />
                    </Col>
                    <Col sm={3} className="footer__links">
                        <Information />
                    </Col>
                    <Col sm={3} className="footer__links">
                        <Support />
                    </Col>
                    <Col sm={3} className="footer__links">
                        <BitisShop />
                    </Col>
                </Row>
            </Container>
        </footer>
        </div>
    );
};
export default Footer;
