function Basket() {
    return (
        <>
            {/* <!--  START Modal basket  --> */}
            <div class="modal-basket-container">
                <input type="checkbox" id="modal-basket-toggle" />
                <label class="modal-backdrop" for="modal-basket-toggle"></label>

                <div class="modal-content">
                    <label class="modal-close-btn" for="modal-basket-toggle">
                        <svg width="50" height="50">
                            <line x1="15" y1="15" x2="35" y2="35" />
                            <line x1="35" y1="15" x2="15" y2="35" />
                        </svg>
                    </label>
                </div>
            </div>
        </>
    )
}
export default Basket;