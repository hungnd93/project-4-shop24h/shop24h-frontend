import './BreadCrumb.css'
import { Breadcrumbs, Link } from "@mui/material";

const BreadCrumb = ({ crumbs }) => {

    return (

        <div className='breadcrumb'>
            <Breadcrumbs separator=">">
                {crumbs.map((item, index) => {
                    return (
                        <Link underline="hover" key={index} style={{ textDecoration: "none", color: "black", textTransform: 'unset' }} href={item.url} >
                            {item.name}
                        </Link>
                    )
                })}
            </Breadcrumbs>
        </div>
    )
}

export default BreadCrumb;
