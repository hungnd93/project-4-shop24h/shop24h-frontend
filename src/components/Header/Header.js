import { onAuthStateChanged, signOut } from 'firebase/auth';
import React, { useState, useRef, useEffect } from 'react';
import { FaBars, FaTimes } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';
import { Button, Dropdown, DropdownItem, DropdownToggle, DropdownMenu } from 'reactstrap';
import auth from '../../firebase';
import './Header.css'

function Header() {

    const navigate = useNavigate();

    const navRef = useRef();

    const showNavbar = () => {
        navRef.current.classList.toggle('responsive_nav');
    }
    const handleClickLogo = () => {
        navigate("/");
    }
    const handleClickLogin = () => {
        if (!user) {
            navigate('/login');
        }
    }

    const [user, setUser] = useState(auth.currentUser);

    const [dropdownOpen, setDropdownOpen] = useState(false);

    const toggleAvatar = () => setDropdownOpen(!dropdownOpen);

    //log out 
    const logoutGoogle = () => {
        signOut(auth)
            .then(() => {
                setUser(null);
            })
            .catch((error) => {
                console.error(error);
            })
    }
    //lưu lại giá trị user khi reload trang
    useEffect(() => {
        onAuthStateChanged(auth, (result) => {
            setUser(result);
        })

    })
    return (

        <div className='main-header'>
            <header className='container'>
                <h3 className='nav-logo' title='Trang chủ' type='button' onClick={handleClickLogo}>Logo</h3>
                <nav ref={navRef}>
                    <a href='/'>Home</a>
                    <a href='/products'>Products</a>
                    <a href='/#'>Blog</a>
                    <a href='/#'>About</a>
                    <button className='nav-btn nav-close-btn' onClick={showNavbar}>
                        <FaTimes />
                    </button>
                </nav>
                <div className='nav-user'>
                    <div className='form-search'>
                        <button className='nav-search-icon'><i class="fas fa-search"></i></button>
                        <input type='text' className='nav-search-text' title='Vui lòng điền vào trường này!'></input>
                    </div>
                    <button className='nav-cart-btn' title='Giỏ hàng'><i class="fas fa-shopping-cart"></i></button>
                    <button className='nav-user-btn' title={user ? user.displayName : 'Tài khoản'} onClick={handleClickLogin}>
                        {
                            user ?
                                <Dropdown nav isOpen={dropdownOpen} toggle={toggleAvatar} style={{ listStyleType: "none" }}>
                                    <DropdownToggle nav >
                                        <img src={user.photoURL} referrerpolicy="no-referrer" style={{ borderRadius: "50%", width: "30px" }} />
                                    </DropdownToggle>
                                    <DropdownMenu className='btn_logout_google'>
                                        <DropdownItem>
                                            Profile
                                        </DropdownItem>
                                        <DropdownItem >
                                            My account
                                        </DropdownItem>
                                        <DropdownItem  onClick={logoutGoogle}>
                                            Logout
                                        </DropdownItem>
                                    </DropdownMenu>
                                </Dropdown>
                                :
                                <div>
                                    <button onClick={handleClickLogin} className="btn-user"><i class="fas fa-user"></i></button>
                                </div>
                        }
                    </button>
                    <button className='nav-btn open-menu-icon' onClick={showNavbar}>
                        <FaBars />
                    </button>
                </div>
            </header>
        </div>
    );
}

export default Header;



