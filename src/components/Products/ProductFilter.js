import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

function ProductFilter() {

    const dispatch = useDispatch();

    const { products, currentPage, totalProduct, allProducts } = useSelector((reduxData) => reduxData.productsReducers);

    const [filter, setFilter] = useState({
        name: '',
        type: '',
        minPrice: '',
        maxPrice: '',
    });

    // const [totalProduct, setTotalProduct] = useState();

    const handleFilterChange = (event) => {
        setFilter({
            ...filter,
            [event.target.name]: event.target.value,
        });

    };

    const filteredProducts = allProducts.filter((product) => {
        return (
            product.Name.toLowerCase().includes(filter.name.toLowerCase()) &&
            (filter.type === '' || product.Type === filter.type) &&
            (filter.minPrice === '' || product.PromotionPrice >= filter.minPrice) &&
            (filter.maxPrice === '' || product.PromotionPrice <= filter.maxPrice)
        );
    });

    return (
        <div className="product-filter">
            <div className="search-filter">
                <i class="fa-solid fa-magnifying-glass fa-lg"></i>
                <input></input>
            </div>
            <div className="filter-name">
                <h5>Tên sản phẩm</h5>
                <ul>
                    <li><a href="">Giày nam</a></li>
                    <li><a href="">Giày nữ</a></li>
                    <li><a href="">Dép</a></li>
                </ul>
            </div>
            <div className="filter-price">
                <h5>Price</h5>
                <div className="filter-input">
                    <input title="min" type='number' value={100} />
                    <div className="separator">-</div>
                    <input title="max" type='number' value={2000} />
                </div>
            </div>
            <div className="product-type">
                <h5>Loại sản phẩm</h5>
                <ul>
                    <li>
                        <label>Giày</label>
                        <input type='checkbox'></input>
                    </li>
                    <li>
                        <label>Dép</label>
                        <input type='checkbox'></input>
                    </li>
                    <li>
                        <label>Túi Xách</label>
                        <input type='checkbox'></input>
                    </li>
                </ul>
            </div>
            <div className="filter-btn">
                <button>Lọc</button>
            </div>
        </div>
    )
}
export default ProductFilter;