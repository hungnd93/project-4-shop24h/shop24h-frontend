import { Pagination, Stack } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changePagination, fetchAllProducts, fetchProducts } from "../../actions/products.actions";

function ProductList() {

    const dispatch = useDispatch();

    const { products, currentPage, totalProduct, allProducts } = useSelector((reduxData) => reduxData.productsReducers);

    const [limit, setLimit] = useState('8');

    const handleChange = (event) => {
        setLimit(event.target.value);
    }

    const noPage = Math.ceil(totalProduct / limit);

    useEffect(() => {
        // Gọi API lấy dữ liệu
        dispatch(fetchProducts(limit, currentPage));
    }, [limit, currentPage]);

    useEffect(() => {
        dispatch(fetchAllProducts())
    }, []);

    const onChangePagination = (event, value) => {
        dispatch(changePagination(value));
    };

    return (
        <div className="product-list-item">
            <div className="product_top_bar d-flex flex-wrap ">
                <div class="sorting mr-auto">
                    <select value={limit} onChange={handleChange}>
                        <option value="8">Show 8</option>
                        <option value="12">Show 12</option>
                        <option value="16">Show 16</option>
                    </select>
                </div>
                <div className="pagination">
                    <Stack spacing={2}>
                        <Pagination count={noPage} defaultPage={currentPage} onChange={onChangePagination} variant="outlined" shape="rounded" color="primary" />
                    </Stack>
                </div>
            </div>
            <div className="product-content">
                {
                    products.map((item, index) => {
                        return (
                            <div class="card" key={index.Id}>
                                <div class="card-img" style={{ backgroundImage: `url(${item.ImageUrl})` }}>
                                    <div class="overlay">
                                        <div class="overlay-content">
                                            <a href="#!">View</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <a href="#!">
                                        <h2>{item.Name}</h2>
                                        <p>{item.Description}</p>
                                    </a>
                                    <div className="price">
                                        <p style={{ color: 'red' }}>{item.PromotionPrice}<sup>đ</sup></p>
                                        <p style={{ color: "#bdc3c7" }}><del>{item.BuyPrice}</del><sup>đ</sup></p>
                                    </div>
                                    <button className="btn btn-light float-end">Add to cart</button>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}
export default ProductList;