import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchProductsLimit } from "../../actions/products.actions";

function NewProduct() {

    const dispatch = useDispatch();

    const { productsLimit } = useSelector((reduxData) => reduxData.productsReducers);

    useEffect(() => {
        dispatch(fetchProductsLimit())
    }, []);

    return (
        <div className="section-item">
            {
                productsLimit.map((item, index) => {
                    return (
                        <div class="card" key={index.Id}>
                            <div class="card-img" style={{ backgroundImage: `url(${item.ImageUrl})` }}>
                                <div class="overlay">
                                    <div class="overlay-content">
                                        <a href="#!">View</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-content">
                                <a href="#!">
                                    <h2>{item.Name}</h2>
                                    <p>{item.Description}</p>
                                </a>
                                <div className="price">
                                    <p style={{ color: 'red' }}>{item.PromotionPrice}<sup>đ</sup></p>
                                    <p style={{ color: "#bdc3c7" }}><del>{item.BuyPrice}</del><sup>đ</sup></p>
                                </div>
                                <button className="btn btn-light float-end">Add to cart <i class="fas fa-plus-circle"></i></button>
                                {/* <i class="fas fa-check-circle"></i> */}
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}
export default NewProduct;