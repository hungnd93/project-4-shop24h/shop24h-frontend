import { useEffect, useState } from "react";
import './DealOfTheWeek.css'
import imgDeal from '../../assets/images/dealofweek.jpg'

function DealOfTheWeek() {
    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

    useEffect(() => {
        const timer = setTimeout(() => {
            setTimeLeft(calculateTimeLeft());
        }, 1000);

        // Clear timeout nếu component bị unmount hoặc thời gian còn lại là 0
        return () => {
            clearTimeout(timer);
        };
    });

    function calculateTimeLeft() {
        const difference = +new Date("2023-03-30") - +new Date();
        let timeLeft = {};

        if (difference > 0) {
            timeLeft = {
                days: Math.floor(difference / (1000 * 60 * 60 * 24)),
                hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
                minutes: Math.floor((difference / 1000 / 60) % 60),
                seconds: Math.floor((difference / 1000) % 60),
            };
        }

        return timeLeft;
    }
    return (
        <div className="deal_ofthe_week">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-6">
                        <div className="deal_ofthe_week_img">
                            <img src={imgDeal} alt="imgdeal" />
                        </div>
                    </div>
                    <div className="col-lg-6 text-right deal_ofthe_week_col">
                        <div className="deal_ofthe_week_content d-flex flex-column align-items-center float-right">
                            <div className="section_title">
                                <h2>Deal Of The Week</h2>
                            </div>
                            <ul className="timer">
                                <li className="d-inline-flex flex-column ">
                                    <div id="day" className="timer_num">{timeLeft.days}</div>
                                    <div className="timer_unit">Day</div>
                                </li>
                                <li className="d-inline-flex flex-column ">
                                    <div id="hour" className="timer_num">{timeLeft.hours}</div>
                                    <div className="timer_unit">Hours</div>
                                </li>
                                <li className="d-inline-flex flex-column ">
                                    <div id="minute" className="timer_num">{timeLeft.minutes}</div>
                                    <div className="timer_unit">Mins</div>
                                </li>
                                <li className="d-inline-flex flex-column">
                                    <div id="second" className="timer_num">{timeLeft.seconds}</div>
                                    <div className="timer_unit">Sec</div>
                                </li>
                            </ul>
                            <div className="red_button deal_ofthe_week_button"><a href="/products">shop now</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default DealOfTheWeek;