
import HomePage from "./pages/HomePage";
import Login from "./pages/LoginPage";
import ProductListPage from "./pages/ProductListPage";

const routerList = [
    {path: "/", element: <HomePage/>},
    {path:"/products", element:<ProductListPage/>},
    {path:"/login", element:<Login/>},
    // {path:"/products/:productId", element:<ProductDetail/>}
]
export default routerList;