import {
    PRODUCTS_FETCH_ERROR,
    PRODUCTS_FETCH_SUCCESS,
    PRODUCTS_FETCH_PENDING,
    PRODUCTS_PAGINATION_CHANGE,
    PRODUCTS_LIMIT_FETCH_SUCCESS,
    PRODUCTS_LIMIT_FETCH_ERROR,
    ALL_PRODUCTS_FETCH_SUCCESS,
    ALL_PRODUCTS_FETCH_ERROR
} from "../constants/products.constants";

const initialState = {
    products: [],
    allProducts: [],
    productsLimit: [],
    pending: false,
    error: null,
    totalProduct: 0,
    currentPage: 1
}

const productsReducers = (state = initialState, action) => {
    switch (action.type) {
        case PRODUCTS_FETCH_PENDING:
            state.pending = true;
            break;
        case PRODUCTS_FETCH_SUCCESS:
            state.totalProduct = action.totalProduct;
            state.pending = false;
            state.products = action.data
            break;
        case PRODUCTS_FETCH_ERROR:
            break;
        case PRODUCTS_PAGINATION_CHANGE:
            state.currentPage = action.page;
            break;
        case PRODUCTS_LIMIT_FETCH_SUCCESS:
            state.pending = false;
            state.productsLimit = action.productsLimit;
            break;
        case PRODUCTS_LIMIT_FETCH_ERROR:
            break;
        case ALL_PRODUCTS_FETCH_SUCCESS:
            state.pending = false;
            state.allProducts = action.dataAllProducts;
            break;
        case ALL_PRODUCTS_FETCH_ERROR:
            break;
        default:
            break;
    }
    return { ...state };
}

export default productsReducers;