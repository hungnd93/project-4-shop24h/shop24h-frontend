import { combineReducers } from "redux";

import productsReducers from "./products.reducers";

const rootReducer = combineReducers({
    productsReducers
});

export default rootReducer;