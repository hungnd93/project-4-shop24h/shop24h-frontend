// import {
//     PRODUCTS_FETCH_ERROR,
//     PRODUCTS_FETCH_PENDING,
//     PRODUCTS_FETCH_SUCCESS,
//     PRODUCTS_LIMIT_FETCH_ERROR,
//     PRODUCTS_LIMIT_FETCH_SUCCESS,
//     PRODUCTS_PAGINATION_CHANGE
// } from "../constants/products.constants";
import * as productContants from '../constants/products.constants';

export const changePagination = (page) => {
    return {
        type: productContants.PRODUCTS_PAGINATION_CHANGE,
        page: page
    }
}

export const fetchProducts = (limit, currentPage) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };

            await dispatch({
                type: productContants.PRODUCTS_FETCH_PENDING
            })
            const responseTotalProduct = await fetch("http://localhost:8000/api/products", requestOptions);

            const dataTotalProduct = await responseTotalProduct.json();

            const response = await fetch("http://localhost:8000/api/products/start/limit?start=" + ((currentPage-1) * limit) + "&limit=" + limit, requestOptions);
            
            const data = await response.json();

            return dispatch({
                type: productContants.PRODUCTS_FETCH_SUCCESS,
                totalProduct: dataTotalProduct.data.length,
                data: data.data
            })
        } catch (error) {
            return dispatch({
                type: productContants.PRODUCTS_FETCH_ERROR,
                error: error
            })
        }

    }
}

export const fetchProductsLimit = () => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };

            await dispatch({
                type: productContants.PRODUCTS_FETCH_PENDING
            })
            const response = await fetch("http://localhost:8000/api/products/limit?limit=12", requestOptions);

            const dataProductsLimit = await response.json();

            return dispatch({
                type: productContants.PRODUCTS_LIMIT_FETCH_SUCCESS,
                productsLimit: dataProductsLimit.data
            })
        } catch (error) {
            return dispatch({
                type: productContants.PRODUCTS_LIMIT_FETCH_ERROR,
                error: error
            })
        }

    }
}

export const fetchAllProducts = () => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };

            await dispatch({
                type: productContants.PRODUCTS_FETCH_PENDING
            })
            const response = await fetch("http://localhost:8000/api/products", requestOptions);

            const dataAllProducts= await response.json();

            return dispatch({
                type: productContants.ALL_PRODUCTS_FETCH_SUCCESS,
                dataAllProducts: dataAllProducts.data
            })
        } catch (error) {
            return dispatch({
                type: productContants.ALL_PRODUCTS_FETCH_ERROR,
                error: error
            })
        }

    }
}
