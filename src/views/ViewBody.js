import { Route, Routes } from "react-router-dom";
import routes from "../routes";

function ViewBody() {
    return (
        <>
            <Routes>
                {routes.map((router, index) => {
                    if (router.path) {
                        return <Route key={index} exact path={router.path} element={router.element}>
                        </Route>
                    }
                })}
            </Routes>
        </>
    )
}
export default ViewBody;