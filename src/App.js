import React from 'react';
import './App.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap/dist/css/bootstrap.css';
import ViewHeader from './views/ViewHeader';
import ViewBody from './views/ViewBody';
import ViewFooter from './views/ViewFooter';

function App() {
  return (
    <React.Fragment>
      <ViewHeader />
      <ViewBody/>
      <ViewFooter/>
    </React.Fragment>
  );
}

export default App;
